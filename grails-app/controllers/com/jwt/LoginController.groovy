package com.jwt

import grails.plugin.springsecurity.annotation.Secured

@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class LoginController {

    def auth() {
        render(view: '/login/auth')
    }

}
