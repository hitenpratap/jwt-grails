package com.jwt

class ApplicationInterceptor {

    ApplicationInterceptor() {
        match(uri: '/*/*/*')
        match(uri: '/*/*')
        match(uri: '/*')
    }

    @Override
    boolean before() {
        println "## Date : ${new Date()}, Params : ${params}"
        return true
    }
}
