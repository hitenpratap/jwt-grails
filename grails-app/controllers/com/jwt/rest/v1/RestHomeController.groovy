package com.jwt.rest.v1

import com.jwt.User
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_PERSON'])
class RestHomeController {

    static namespace = "version1"

    def springSecurityService

    def index() {
        User user = springSecurityService.currentUser as User
        def result = [:]
        result.put("status", "SUCCESS")
        result.put("username", "${user?.username}")
        result.put("fullName", "${user?.fullName}")
        result.put("message", "Welcome to the fascinating world of JWT!")
        render result as JSON
    }

}
