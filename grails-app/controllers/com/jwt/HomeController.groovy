package com.jwt

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_PERSON'])
class HomeController {

    def springSecurityService

    def index() {
        User user = springSecurityService.currentUser as User
        render(view: '/home/index', model: [user: user])
    }

}
