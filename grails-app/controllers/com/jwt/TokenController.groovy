package com.jwt

import grails.plugin.springsecurity.annotation.Secured
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import org.apache.commons.lang.RandomStringUtils

import java.text.SimpleDateFormat

@Secured(['ROLE_ADMIN', 'ROLE_PERSON'])
class TokenController {

    def tokenService
    def springSecurityService

    def tokenForm = {
        User user = springSecurityService.currentUser as User
        render(view: '/token/form', model: [user: user, tokenId: RandomStringUtils.randomAlphanumeric(9)])
    }

    def submitTokenForm = {
        if (params.subject && request.method == 'POST') {
            User user = springSecurityService.currentUser as User
            Map<String, String> payloadMap = [:]
            if (params.payload) {
                def stringArr = params.payload.split(",")
                stringArr.each { String data ->
                    def mapStrdata = data.split(":")
                    payloadMap.put(mapStrdata[0], mapStrdata[1])
                }
            }
            Date expiration = null
            if (params.expiration) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm")
                expiration = dateFormat.parse(params.expiration)
            }
            String jwtToken = tokenService.generateJWTToken(params.subject, params.tokenId, payloadMap, user, expiration)
            render(view: '/token/tokenShow', model: [user: user, jwtToken: jwtToken])
        } else {
            redirect(action: 'tokenForm')
        }
    }

    def parseTokenForm = {
        User user = springSecurityService.currentUser as User
        render(view: '/token/tokenParse', model: [user: user])
    }

    def processParseJWTToken = {
        if (params.jwtToken && request.method == "POST") {
            User user = springSecurityService.currentUser as User
            Jws<Claims> claims = tokenService.parseJWTToken(params.jwtToken)
            def claimDataMap = claims?.body as Map<String, String>
            if (claimDataMap) {
                ["iss", "iat", "jti"].each {
                    if (claimDataMap.containsKey(it))
                        claimDataMap.remove(it)
                }
            }
            render(view: '/token/tokenDetails', model: [jwtToken: params.jwtToken, user: user, claims: claims, claimDataMap: claimDataMap])
        } else {
            redirect(action: 'tokenForm')
        }
    }

}
