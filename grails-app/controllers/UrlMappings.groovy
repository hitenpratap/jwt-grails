class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "public", action: "index")
        "500"(view: '/error')
        "404"(view: '/notFound')

        "/api/v1/rest-home/$action?/$uniqueId?(.${format})?" {
            controller = 'restHome'
            namespace = 'version1'
        }
    }
}
