<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><g:layoutTitle default="JWT"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Atomic+Age" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <asset:stylesheet href="application.css"/>
</head>

<body>
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand main-logo" href="${createLink(uri: '/')}">JWT Demo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                %{--<li class="nav-item active">--}%
                %{--<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>--}%
                %{--</li>--}%
                %{--<li class="nav-item">--}%
                %{--<a class="nav-link" href="#">Link</a>--}%
                %{--</li>--}%
                %{--<li class="nav-item">--}%
                %{--<a class="nav-link disabled" href="#">Disabled</a>--}%
                %{--</li>--}%
            </ul>

            <form class="form-inline mt-2 mt-md-0">
                <ul class="navbar-nav mr-auto">
                    <sec:ifLoggedIn>
                        <li class="nav-item active">
                            <a class="nav-link" href="${createLink(controller: 'home', action: 'index')}"><i
                                    class="fa fa-user"></i> <sec:username/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="${createLink(controller: 'token', action: 'tokenForm')}"><i
                                    class="fa fa-globe"></i> Generate Token</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="${createLink(controller: 'token', action: 'parseTokenForm')}"><i
                                    class="fa fa-globe"></i> Parse Token</a>
                        </li>
                    </sec:ifLoggedIn>
                    <li class="nav-item">
                        <sec:ifLoggedIn>
                            <a class="nav-link" href="${createLink(controller: 'logout')}"><i
                                    class="fa fa-sign-out"></i> Logout</a>
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <a class="nav-link" href="${createLink(controller: 'login', action: 'auth')}"><i
                                    class="fa fa-sign-in"></i> Login</a>
                        </sec:ifNotLoggedIn>
                    </li>
                </ul>
            </form>
        </div>
    </nav>
</header>
<main role="main" class="container">
    <br/><br/>
    <g:layoutBody/>
    <br/><br/>
</main>
<footer class="footer">
    <div class="container">
        <p class="text-muted text-center"><i class="fa fa-copyright"></i> JWT</p>
    </div>
</footer>
</body>
</html>
