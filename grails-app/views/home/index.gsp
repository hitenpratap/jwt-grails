<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <h2 class="card-title">${user?.fullName}</h2>

        <p class="card-text">Welcome to Dashboard.</p>
        <a href="${createLink(uri: '/')}" class="btn btn-primary">Go somewhere</a>
    </div>
</div>
</body>
</html>