<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <h2 class="card-title">Welcome to the fascinating world of JWT!</h2>

        <p class="card-text">JSON Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties..
        </p>
        <a href="${createLink(controller: 'token', action: 'tokenForm')}" class="btn btn-primary">Go Dive In!</a>
    </div>
</div>
</body>
</html>