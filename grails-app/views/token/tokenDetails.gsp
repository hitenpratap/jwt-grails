<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <h2 class="card-title">${user?.fullName} your token details are here!</h2>

        <div class="form-group">
            <textarea class="form-control" name="payload" id="payload" rows="6" disabled>${jwtToken}</textarea>
        </div>

        <a href="${createLink(controller: 'token', action: 'tokenForm')}"
           class="btn btn-primary">Generate More Tokens!</a>
    </div>
</div>
<br/>

<div class="card">
    <div class="card-body">
        <h2 class="card-title">JWT Token broken into parts!</h2>

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <tr>
                    <th>Header</th>
                    <td>${claims?.header}</td>
                </tr>
                <tr>
                    <th>payload</th>
                    <td>${claims?.body}</td>
                </tr>
                <tr>
                    <th>Signature</th>
                    <td>${claims?.signature}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br/>

<div class="card">
    <div class="card-body">
        <h2 class="card-title">JWT Token Details!</h2>

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <tr>
                    <th>Subject</th>
                    <td>${claims?.body?.getSubject()}</td>
                </tr>
                <tr>
                    <th>Expiration</th>
                    <td>${claims?.body?.getExpiration() ?: 'N/A'}</td>
                </tr>
                <tr>
                    <th>Issuer</th>
                    <td>${claims?.body?.getIssuer()}</td>
                </tr>
                <tr>
                    <th>Issued At</th>
                    <td>${claims?.body?.getIssuedAt()?.format("MMM dd, yyyy hh:mm")}</td>
                </tr>
                <g:each in="${claimDataMap?.keySet()}" var="claimKey">
                    <tr>
                        <th>${claimKey}</th>
                        <td>${claimDataMap.get(claimKey)}</td>
                    </tr>
                </g:each>
            </table>
        </div>
    </div>
</div>
</body>
</html>