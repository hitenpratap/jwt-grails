<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="card-title">Welcome, ${user?.fullName}!</h2>
            </div>

            <div class="col-lg-6">
                <h2 class="card-title pull-right">Token Parse</h2>
            </div>
        </div>

        <form action="${createLink(controller: 'token', action: 'processParseJWTToken')}" method="POST"
              autocomplete="off">
            <div class="row">
                <div class="form-group col-lg-12">
                    <label for="jwtToken">
                        JWT Token
                    </label>
                    <textarea class="form-control" name="jwtToken" id="jwtToken" rows="6"></textarea>
                    <small class="form-text text-muted">Paste your JWT token here generated from previous step!</small>
                </div>

                <div class="form-group col-lg-12">
                    <button type="submit" class="btn btn-primary" tabindex="4" id="submit">
                        Submit
                    </button>
                    <button type="reset" class="btn btn-light" tabindex="4" id="reset">
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>