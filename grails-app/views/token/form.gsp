<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="card-title">Welcome, ${user?.fullName}!</h2>
            </div>

            <div class="col-lg-6">
                <h2 class="card-title pull-right">Token Generation</h2>
            </div>
        </div>

        <form action="${createLink(controller: 'token', action: 'submitTokenForm')}" method="POST" autocomplete="off">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label for="subject">
                        Subject
                    </label>
                    <input type="text" class="form-control" name="subject" id="subject"
                           placeholder="Enter Subject" tabindex="1">
                </div>

                <div class="form-group col-lg-4">
                    <label for="subject">
                        Expiration
                    </label>
                    <input type="datetime-local" class="form-control" name="expiration" id="expiration"
                           tabindex="1">
                </div>

                <div class="form-group col-lg-2">
                    <label for="tokenId">
                        Token Id
                    </label>
                    <input type="text" class="form-control" name="tokenId" id="tokenId"
                           tabindex="1" value="${tokenId}">
                </div>

                <div class="form-group col-lg-12">
                    <label for="payload">
                        Payload
                    </label>
                    <textarea class="form-control" name="payload" id="payload" rows="6"></textarea>
                    <small class="form-text text-muted">Pair of key & value separate by , i.e. <code>"name1":"value1","name2":"value2"</code>
                    </small>
                </div>

                <div class="form-group col-lg-12">
                    <button type="submit" class="btn btn-primary" tabindex="4" id="submit">
                        Submit
                    </button>
                    <button type="reset" class="btn btn-light" tabindex="4" id="reset">
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>