<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <h2 class="card-title">${user?.fullName} your token is ready to use!</h2>

        <div class="form-group">
            <textarea class="form-control" name="payload" id="payload" rows="6" disabled>${jwtToken}</textarea>
        </div>
        <a href="${createLink(controller: 'token', action: 'tokenForm')}"
           class="btn btn-primary">Generate More Tokens!</a>
        <a href="${createLink(controller: 'token', action: 'parseTokenForm')}"
           class="btn btn-success pull-right">Parse JWT Token!</a>
    </div>
</div>
</body>
</html>