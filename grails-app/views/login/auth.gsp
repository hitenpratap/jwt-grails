<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<div class="card">
    <div class="card-body">
        <h2 class="card-title">Login to get started!</h2>

        <form action="${'/login/authenticate'}" method="POST" id="loginForm" autocomplete="off">
            <div class="form-group">
                <label for="username">
                    Email Address
                </label>
                <input type="email" class="form-control" name="username" id="username"
                       placeholder="Enter email" tabindex="1">
            </div>

            <div class="form-group">
                <label for="password">
                    Password
                </label>
                <input type="password" class="form-control" name="password" id="password"
                       placeholder="Enter Password" tabindex="2">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary" tabindex="4" id="submit">
                    Submit
                </button>
                <button type="reset" class="btn btn-light" tabindex="4" id="reset">
                    Reset
                </button>
            </div>
        </form>
    </div>
</div>
</body>
</html>