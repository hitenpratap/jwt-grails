package com.jwt

import grails.util.Holders
import io.jsonwebtoken.*

class TokenService {


    final String SECRET_SIGNATURE = Holders.config.jwt.secret.signature

    String generateJWTToken(String subject, String tokenId, Map<String, Object> claimMap, User user, Date expiration = null) {
        JwtBuilder jwtTokenBuilder = Jwts.builder()
                .setSubject(subject)
                .setIssuer(user?.username)
                .setIssuedAt(new Date())
                .setId(tokenId)
                .signWith(
                SignatureAlgorithm.HS256,
                "${SECRET_SIGNATURE}".getBytes("UTF-8")
        )
        if (expiration)
            jwtTokenBuilder.setExpiration(expiration)
        claimMap.each { String key, Object value ->
            jwtTokenBuilder.claim(key, value)
        }
        String jwtToken = jwtTokenBuilder.compact()
        println("JWT Token      ===========>>>>>>>>>        ${jwtToken}")
        jwtToken
    }

    Jws<Claims> parseJWTToken(String token) {
        Jws<Claims> claims = null
        try {
            claims = Jwts.parser()
                    .setSigningKey("${SECRET_SIGNATURE}".getBytes("UTF-8"))
                    .parseClaimsJws(token)
        } catch (PrematureJwtException pje) {
            //JWT was accepted before it is allowed to be accessed and must be rejected
            pje.printStackTrace()
        } catch (ExpiredJwtException eje) {
            //JWT was accepted after it expired and must be rejected
            eje.printStackTrace()
        } catch (ClaimJwtException cje) {
            //Validation of a JWT claim failed
            cje.printStackTrace()
        } catch (MalformedJwtException mje) {
            //JWT was not correctly constructed and should be rejected
            mje.printStackTrace()
        } catch (SignatureException se) {
            //Either calculating a signature or verifying an existing signature of a JWT failed
            se.printStackTrace()
        } catch (UnsupportedJwtException uje) {
            //JWT in a particular format/configuration that does not match the format expected by the application
            uje.printStackTrace()
        } catch (Exception e) {
            e.printStackTrace()
        }
        claims
    }

}
