package com.jwt

class BootstrapService {

    public void bootstrapData() {
        createRoles()
        createAdmin()
        createPerson()
    }

    def createRoles() {
        Role.countByAuthority("ROLE_ADMIN") ?: new Role(authority: "ROLE_ADMIN").save(flush: true)
        Role.countByAuthority("ROLE_PERSON") ?: new Role(authority: "ROLE_PERSON").save(flush: true)
    }

    def createAdmin() {
        Role roleAdmin = Role.findByAuthority("ROLE_ADMIN")
        if (!Admin.countByUsername("admin1@email.com")) {
            Admin admin = new Admin(username: "admin1@email.com", password: "admin1234")
            admin.save(flush: true)
            UserRole.create(admin, roleAdmin, true)
        }
        if (!Admin.countByUsername("admin2@email.com")) {
            Admin admin = new Admin(username: "admin2@email.com", password: "admin1234")
            admin.save(flush: true)
            UserRole.create(admin, roleAdmin, true)
        }
    }

    def createPerson() {
        Role rolePerson = Role.findByAuthority("ROLE_PERSON")
        if (!Person.countByUsername("person1@email.com")) {
            Person person = new Person(firstName: "Test", lastName: "Person #1", username: "person1@email.com", password: "person1234")
            person.save(flush: true)
            UserRole.create(person, rolePerson, true)
        }
        if (!Person.countByUsername("person2@email.com")) {
            Person person = new Person(firstName: "Test", lastName: "Person #2", username: "person2@email.com", password: "person1234")
            person.save(flush: true)
            UserRole.create(person, rolePerson, true)
        }
    }

}
