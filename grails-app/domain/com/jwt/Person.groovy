package com.jwt

class Person extends User {

    String firstName
    String lastName

    static transients = ['fullName']

    String getFullName() {
        this.firstName + " " + this.lastName
    }

}
