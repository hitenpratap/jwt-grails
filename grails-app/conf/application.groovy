// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.jwt.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.jwt.UserRole'
grails.plugin.springsecurity.authority.className = 'com.jwt.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}

environments {
    development {
        dataSource {
            dbCreate = "create-drop"
            url = "jdbc:mysql://localhost:3306/jwt_demo?autoreconnect=true"
            username = "root"
            logSql = false
            password = "nextdefault"
        }
    }
}

grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/home/index'
grails.plugin.springsecurity.auth.loginFormUrl = '/login/auth'


grails.plugin.springsecurity.rest.login.active = true
grails.plugin.springsecurity.rest.login.endpointUrl = '/api/login'
grails.plugin.springsecurity.rest.login.failureStatusCode = 401
grails.plugin.springsecurity.rest.login.useJsonCredentials = true
grails.plugin.springsecurity.rest.login.usernamePropertyName = 'username'
grails.plugin.springsecurity.rest.login.passwordPropertyName = 'password'
grails.plugin.springsecurity.rest.logout.endpointUrl = '/api/logout'
grails.plugin.springsecurity.rest.token.storage.useGorm = false
//grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = "com.bankScraper.AuthenticationToken"
//grails.plugin.springsecurity.rest.token.storage.gorm.tokenValuePropertyName = "tokenValue"
//grails.plugin.springsecurity.rest.token.storage.gorm.usernamePropertyName = 'username'
grails.plugin.springsecurity.rest.login.usernamePropertyName = 'username'
grails.plugin.springsecurity.rest.login.passwordPropertyName = 'password'
grails.plugin.springsecurity.rest.token.generation.useSecureRandom = true
grails.plugin.springsecurity.rest.token.validation.endpointUrl = "/api/validate"
grails.plugin.springsecurity.rest.token.validation.headerName = "X-Auth-Token"
grails.plugin.springsecurity.rest.token.validation.active = true
grails.plugin.springsecurity.conf.rest.token.storage.jwt.secret = "jdbckb38964r3bf#\$#@^#^fjebviue#%jfbv#@rjdbvc3874r6bdjks#%575jdbsv#@%\$#jkdfv"
grails.plugin.springsecurity.rest.token.storage.jwt.useSignedJwt = true
grails.plugin.springsecurity.rest.token.storage.jwt.expiration = 3600
//grails.plugin.springsecurity.rest.token.validation.useBearerToken = false
//grails.plugin.springsecurity.rest.token.rendering.tokenPropertyName = "access_token"

grails {
    plugin {
        springsecurity {
            filterChain {
                chainMap = [
//         ,-restTokenValidationFilter,-restExceptionTranslationFilter,-restLogoutFilter']
//        [pattern: '/api/**', filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,' +
//                '-securityContextPersistenceFilter,-rememberMeAuthenticationFilter,-restLogoutFilter']
[pattern: '/api/guest/**', filters: 'anonymousAuthenticationFilter,restTokenValidationFilter,restExceptionTranslationFilter,filterInvocationInterceptor'],
[pattern: '/api/v1/rest-home/**', filters: 'anonymousAuthenticationFilter,restTokenValidationFilter,restExceptionTranslationFilter,filterInvocationInterceptor'],
[
        pattern: '/api/**',
        filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'
],
[pattern: '/assets/**', filters: 'none'],
[pattern: '/**/js/**', filters: 'none'],
[pattern: '/**/css/**', filters: 'none'],
[pattern: '/**/images/**', filters: 'none'],
[pattern: '/**/favicon.ico', filters: 'none'],
//Traditional chain
[
        pattern: '/**',
        filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'
]
                ]
            }
            //Other Spring Security settings
            //...
            rest {
                token {
                    validation {
                        enableAnonymousAccess = true
                    }
                }
            }
        }
    }
}

jwt.secret.signature = "cXGbF#L^&DXZ4d2BEL4B\$v-QBPvwz^vFw2?Jc+h4pQrfVa+Bhs%XWpYfb%!xmPpBW\$%es8aF4K2MEB!7m5Ac+QA=8cWUh7Yr=LhksZ%R&*5-xBM"
